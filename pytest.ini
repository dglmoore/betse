# --------------------( LICENSE                            )--------------------
# Copyright 2014-2017 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.
#
# --------------------( SYNOPSIS                           )--------------------
# Project-wide "pytest.ini" configuration, applied to all invocations of
# the py.test test runner within this project -- including both by directly
# running the external "py.test" command and by indirectly running the
# setuptools-harnessed "python3 setup.py test" subcommand.
#
# --------------------( DETAILS                            )--------------------
# To permit tests to transparently import from the main non-test codebase, this
# file resides in the root project directory containing top-level subdirectories
# "betse" and "betse_test". py.test then:
#
# 1. Recursively finds this file.
# 2. Sets "config.inifile" to the absolute path of this file.
# 3. Sets "config.rootdir" to the absolute path of this file's directory.
#
# See https://pytest.org/latest/customize.html for details.

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# NOTE: For a list of all BETSE-specific CLI options unconditionally passed to
# all invocations of the external "betse" command by functional tests, see the
# "betse_test.func.cli.fixture.clier" submodule.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

# ....................{ BOILERPLATE                        }....................
# The following py.test-specific section specifier is mandatory, despite this
# file's unambiguous basename of "pytest.ini". One is enraged by bureaucracy!
[pytest]

# ....................{ OPTIONS                            }....................
#FIXME: Conditionally support the following plugin-based options in an
#appropriate setuptools command when the requisite plugin is importable:
#
#* "--instafail", immediately printing test output rather than delaying such
#  output until after all tests complete. This requires the "pytest-instafail"
#  plugin. Note that this may not necessarily play nicely with the "--capture=no"
#  option leveraged below. Consider further testing (...get it!).
#FIXME: Pass "--ff" and "--tb=auto" when all test machines have a sufficiently
#new version of py.test installed.

# Unconditionally pass the following command-line options to all invocations of
# the "py.test" command. Dismantled, this is:
#
# * "-v", increasing verbosity.
# * "-r a", increasing verbosity of (a)ll types of test summaries.
# * "--doctest-glob=", disabling implicit detection of doctests (i.e., tests
#   embedded in docstrings that double as human-readable examples). By default,
#   py.test runs all files matching the recursive glob "**/test*.txt" through
#   the standard "doctest" module. Since BETSE employs explicit tests rather
#   than implicit doctests, such detection is a non-fatal noop in the best case
#   and a fatal conflict in the worst case. Disable such detection.
# * "--failed-first", prioritizing tests that failed ahead of tests that
#   succeeded on the most recent test run. Actually, this option has been
#   temporarily omitted. Why? Because serial tests currently fail to implicitly
#   require prerequisite tests (e.g., "test_cli_sim_default[sim]" fails to
#   require "test_cli_sim_default[seed]"), thus requiring that tests be run
#   *ONLY* in the default ordering.
# * "--maxfail=...", halting testing after the passed number of test failures.
# * "--showlocals", printing local variable values in tracebacks.
# * "--tb=auto", printing "long" tracebacks for the first and last trace stack
#   frames but "short" tracebacks for all other stack frames.
#
# See "py.test --help | less" for further details.
addopts = -v -r a --doctest-glob= --showlocals --tb=auto

# Minimum version of py.test required by:
#
# * The "--failed-first" option enabled by default above.
# minversion = 2.8.0

# Whitespace-delimited list of the relative paths of all top-level directories
# containing tests. All Python scripts with basenames prefixed by "test_" in all
# subdirectories of these directories including these directories themselves
# will be parsed for:
#
# * Functions whose names are prefixed by "test_".
# * Classes whose names are prefixed by "Test".
testpaths = betse_test
