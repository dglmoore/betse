# --------------------( LICENSE                            )--------------------
# Copyright 2014-2017 by Alexis Pietak & Cecil Curry.
# See "LICENSE" for further details.
#
# --------------------( SYNOPSIS                           )--------------------
# Project-wide pip configuration. When installing this package via pip, each
# uncommented line of this file lists:
#
# * A command-line option to be passed to the "pip install" command, prefixed
#   by either "-" or "--".
# * A package dependency to be installed by pip.

# ....................{ OPTIONS                            }....................
# This package currently requires no custom options.

# ....................{ DEPENDENCIES                       }....................
# Default to the set of all mandatory and optional runtime dependencies listed
# by the top-level "setup.py" script. Dismantled, this is:
#
# * ".", installing the current application from script *AND* all mandatory
#   runtime dependencies of this application #   declared by this script's
#   "install_requires" key.
# * "[all]", installing all optional runtime dependencies of this application
#   declared by the "all" key of this script's "extras_require" key. While
#   optional dependencies are of course optional, increasing these
#   dependencies maximizes test coverage and hence are effectively mandatory.
.[all]
